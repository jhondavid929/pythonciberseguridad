
# -*- coding: latin-1 -*-

import hashlib


def fuerza_Bruta():
    intro_hash = input("Introduce la contrasela encriptada: ")

    dicc_contrasenia = open("HASH_diccionario.txt","r")
    
    for contrasenia  in dicc_contrasenia.readlines():
        contrasenia = contrasenia.strip()
        Encriptado_contr = hashlib.sha1(contrasenia.encode()).hexdigest()
        if intro_hash == Encriptado_contr:
            print("Contraseña encontrada: " + contrasenia)
            break
        else:
            print("Contraseña NO encontrada")
fuerza_Bruta()